' *** Author: aker ***

Option Explicit

Const LngMinValue = -2147483648
Const LngMaxValue = 2147483647

Dim strInFile, strOutFile, strOffset, strLength ' String
Dim numOffset, numLength ' Long
Dim objFileSystem ' Scripting.FileSystemObject
Dim objInFile, objOutFile ' ADODB.Stream

Private Sub CreateDirectoryStructure(strTargetFilePath)
  Dim arrPath, tmpPath, i
  
  If (Not objFileSystem Is Nothing) Then
    arrPath = Split(strTargetFilePath, "\")
    
    tmpPath = ""
    
    For i = 0 To (UBound(arrPath) - 1) ' last part of the array is the file name; do not create a folder for it
      If (arrPath(i) <> "") Then
        If tmpPath <> "" Then
          tmpPath = tmpPath & "\" & arrPath(i)
        Else
          tmpPath = arrPath(i)
        End If
      End If
      If objFileSystem.FolderExists(tmpPath) = False Then
        objFileSystem.CreateFolder(tmpPath)
      End If
    Next
  End If
End Sub

' ConnectModeEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/connectmodeenum)
' 0 = adModeUnknown
' 1 = adModeRead
' 2 = adModeWrite
' 3 = 1 | 2 = adModeReadWrite
' 4 = adModeShareDenyRead
' 8 = adModeShareDenyWrite
' 12 = 8 | 4 = adModeShareExclusive
' 16 = adModeShareDenyNone
' 0x400000 = adModeRecursive

' StreamTypeEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/streamtypeenum)
' 1 = adTypeBinary
' 2 = adTypeText

' SaveOptionsEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/saveoptionsenum)
' 1 = adSaveCreateNotExist
' 2 = adSaveCreateOverWrite

If WScript.Arguments.Count <> 4 Then
  WScript.Echo("ERROR: Missing argument.")
  WScript.Echo("Usage: " & WScript.ScriptName & " <input file> <output file> <offset> <length>")
  WScript.Quit(1)
End If

strInFile = WScript.Arguments(0)
strOutFile = WScript.Arguments(1)
strOffset = WScript.Arguments(2)
strLength = WScript.Arguments(3)

Set objFileSystem = CreateObject("Scripting.FileSystemObject")

If objFileSystem.FileExists(strInFile) = False Then
  WScript.Echo("ERROR: Input file does not exist.")
  WScript.Quit(1)
End If

If IsNumeric(strOffset) = False Then
  WScript.Echo("ERROR: Invalid offset.")
  WScript.Quit(1)
End If
If IsNumeric(strLength) = False Then
  WScript.Echo("ERROR: Invalid length.")
  WScript.Quit(1)
End If

numOffset = CLng(strOffset)
numLength = CLng(strLength)

If CStr(numOffset) <> strOffset Then
  WScript.Echo("ERROR: Failed to parse offset.")
  WScript.Quit(1)
End If
If CStr(numLength) <> strLength Then
  WScript.Echo("ERROR: Failed to parse length.")
  WScript.Quit(1)
End If

If numOffset < 0 Then
  WScript.Echo("ERROR: Offset must be greater than or equal to 0.")
  WScript.Quit(1)
End If
If numLength <= 0 Then
  WScript.Echo("ERROR: Length must be greater than 0.")
  WScript.Quit(1)
End If
' Catch case "Overflow bei numOffset + numLength > Long.MaxValue"
If (LngMaxValue - numOffset) < numLength Then
  WScript.Echo("ERROR: Specified offset and length might cause an overflow.")
  WScript.Quit(1)
End If

Set objInFile = CreateObject("ADODB.Stream")

'objInFile.Mode = (1 Or 12) ' ConnectModeEnum
objInFile.Type = 1 ' StreamTypeEnum
objInFile.Open
objInFile.LoadFromFile strInFile

If numOffset >= objInFile.Size Then
  objInFile.Close
  WScript.Echo("ERROR: Start offset is out of bounds.")
  WScript.Quit(1)
End If
If (numOffset + numLength) >= objInFile.Size Then
  objInFile.Close
  WScript.Echo("ERROR: End offset is out of bounds.")
  WScript.Quit(1)
End If

Set objOutFile = CreateObject("ADODB.Stream")
'objOutFile.Mode = (2 Or 12) ' ConnectModeEnum
objOutFile.Type = 1 ' StreamTypeEnum
objOutFile.Open

objInFile.Position = numOffset
objOutFile.Position = 0
objInFile.CopyTo objOutFile, numLength

CreateDirectoryStructure(strOutFile)

objOutFile.SaveToFile strOutFile, 2

objOutFile.Close

objInFile.Close

WScript.Quit(0)