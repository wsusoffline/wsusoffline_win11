' *** Author: aker ***

Option Explicit

Const LngMinValue = -2147483648
Const LngMaxValue = 2147483647

Dim strInFile, strOutFolder, strOffsetFile, strOffsetFileLine, strOffset, strLength ' String
Dim arrOffsetFileLine ' String-Array
Dim numOffset, numLength ' Long
Dim cntCurrent ' Integer/Long
Dim objFileSystem ' Scripting.FileSystemObject
Dim objOffsetFile ' Scripting.Dictionary
Dim objInFile, objOutFile ' ADODB.Stream
Dim i ' Integer
Dim bDebugMode ' Boolean

' Quelle: https://stackoverflow.com/questions/854975/how-to-read-from-a-text-file-using-vbscript
Private Function ReadTextFile(strRelFileName)
  Dim dict, file, line, row
  
  Set dict = CreateObject("Scripting.Dictionary")
  
  If ((Not objFileSystem Is Nothing) And (objFileSystem.FileExists(strRelFileName) = True)) Then
    Set file = objFileSystem.OpenTextFile (strRelFileName)
    row = 0
    Do Until file.AtEndOfStream
      line = file.Readline
      dict.Add row, line
      row = row + 1
    Loop
    
    file.Close
  End If
  
  Set ReadTextFile = dict
End Function

Private Sub CreateDirectoryStructure(strTargetFilePath)
  Dim arrPath, tmpPath, i
  
  If (Not objFileSystem Is Nothing) Then
    arrPath = Split(strTargetFilePath, "\")
    
    tmpPath = ""
    
    For i = 0 To (UBound(arrPath) - 1) ' last part of the array is the file name; do not create a folder for it
      If (arrPath(i) <> "") Then
        If tmpPath <> "" Then
          tmpPath = tmpPath & "\" & arrPath(i)
        Else
          tmpPath = arrPath(i)
        End If
      End If
      If objFileSystem.FolderExists(tmpPath) = False Then
        objFileSystem.CreateFolder(tmpPath)
      End If
    Next
  End If
End Sub

' ConnectModeEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/connectmodeenum)
' 0 = adModeUnknown
' 1 = adModeRead
' 2 = adModeWrite
' 3 = 1 | 2 = adModeReadWrite
' 4 = adModeShareDenyRead
' 8 = adModeShareDenyWrite
' 12 = 8 | 4 = adModeShareExclusive
' 16 = adModeShareDenyNone
' 0x400000 = adModeRecursive

' StreamTypeEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/streamtypeenum)
' 1 = adTypeBinary
' 2 = adTypeText

' SaveOptionsEnum (https://docs.microsoft.com/en-us/sql/ado/reference/ado-api/saveoptionsenum)
' 1 = adSaveCreateNotExist
' 2 = adSaveCreateOverWrite

If WScript.Arguments.Count < 3 Then
  WScript.Echo("ERROR: Missing argument.")
  WScript.Echo("Usage: " & WScript.ScriptName & " <input file> <output folder> <file list> [/debug]")
  WScript.Quit(1)
End If

strInFile = WScript.Arguments(0)
strOutFolder = WScript.Arguments(1)
strOffsetFile = WScript.Arguments(2)
bDebugMode = False

If WScript.Arguments.Count > 3 Then
  For i = 3 To (WScript.Arguments.Count - 1)
    If WScript.Arguments(i) = "/debug" Then
      bDebugMode = True
    End If
  Next
End If

Set objFileSystem = CreateObject("Scripting.FileSystemObject")

If objFileSystem.FileExists(strInFile) = False Then
  WScript.Echo("ERROR: Input file does not exist.")
  WScript.Quit(1)
End If
If objFileSystem.FileExists(strOffsetFile) = False Then
  WScript.Echo("ERROR: Input file does not exist.")
  WScript.Quit(1)
End If

Set objInFile = CreateObject("ADODB.Stream")

'objInFile.Mode = (1 Or 12) ' ConnectModeEnum
objInFile.Type = 1 ' StreamTypeEnum
objInFile.Open
objInFile.LoadFromFile strInFile

Set objOffsetFile = ReadTextFile(strOffsetFile)

cntCurrent = 0
For Each strOffsetFileLine In objOffsetFile.Items
  cntCurrent = cntCurrent + 1
  If bDebugMode = True Then
    WScript.Echo("Extracting file " + CStr(cntCurrent) + + " of " + CStr(objOffsetFile.Count) + "...")
  End If
  If strOffsetFileLine <> "" Then
    arrOffsetFileLine = Split(strOffsetFileLine, ",")
    If UBound(arrOffsetFileLine) <> 2 Then
      objInFile.Close
      WScript.Echo("ERROR: Failed to parse file list.")
      WScript.Quit(1)
    End If
    ' arrOffsetFileLine(0) = file name
    ' arrOffsetFileLine(1) = offset
    ' arrOffsetFileLine(2) = length
    
    strOffset = arrOffsetFileLine(1)
    strLength = arrOffsetFileLine(2)
    
    If IsNumeric(strOffset) = False Then
      objInFile.Close
      WScript.Echo("ERROR: Invalid offset.")
      WScript.Quit(1)
    End If
    If IsNumeric(strLength) = False Then
      objInFile.Close
      WScript.Echo("ERROR: Invalid length.")
      WScript.Quit(1)
    End If
    
    numOffset = CLng(strOffset)
    numLength = CLng(strLength)
    
    If CStr(numOffset) <> strOffset Then
      objInFile.Close
      WScript.Echo("ERROR: Failed to parse offset.")
      WScript.Quit(1)
    End If
    If CStr(numLength) <> strLength Then
      objInFile.Close
      WScript.Echo("ERROR: Failed to parse length.")
      WScript.Quit(1)
    End If
    
    If numOffset < 0 Then
      objInFile.Close
      WScript.Echo("ERROR: Offset must be greater than or equal to 0.")
      WScript.Quit(1)
    End If
    If numLength <= 0 Then
      objInFile.Close
      WScript.Echo("ERROR: Length must be greater than 0.")
      WScript.Quit(1)
    End If
    ' Catch case "Overflow bei numOffset + numLength > Long.MaxValue"
    If (LngMaxValue - numOffset) < numLength Then
      objInFile.Close
      WScript.Echo("ERROR: Specified offset and length might cause an overflow.")
      WScript.Quit(1)
    End If
    
    If numOffset >= objInFile.Size Then
      objInFile.Close
      WScript.Echo("ERROR: Start offset is out of bounds.")
      WScript.Quit(1)
    End If
    If (numOffset + numLength) >= objInFile.Size Then
      objInFile.Close
      WScript.Echo("ERROR: End offset is out of bounds.")
      WScript.Quit(1)
    End If
    
    Set objOutFile = CreateObject("ADODB.Stream")
    'objOutFile.Mode = (2 Or 12) ' ConnectModeEnum
    objOutFile.Type = 1 ' StreamTypeEnum
    objOutFile.Open
    
    objInFile.Position = numOffset
	objOutFile.Position = 0
    objInFile.CopyTo objOutFile, numLength
    
    CreateDirectoryStructure((strOutFolder + "\" + arrOffsetFileLine(0)))
    
    objOutFile.SaveToFile (strOutFolder + "\" + arrOffsetFileLine(0)), 2
    
    objOutFile.Close
  End If
Next

objInFile.Close

WScript.Quit(0)