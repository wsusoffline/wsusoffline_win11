; ***               UnpackPA30               ***
; ***              Author: aker              ***

#AutoIt3Wrapper_Change2CUI=y

#include <MsgBoxConstants.au3>
#include <WinAPIError.au3>

#pragma compile(CompanyName, "aker")
#pragma compile(FileDescription, "UnpackPA30")
#pragma compile(FileVersion, 1.0.0)
#pragma compile(InternalName, "UnpackPA30")
#pragma compile(LegalCopyright, "GNU GPLv3")
#pragma compile(OriginalFilename, UnpackPA30.exe)
#pragma compile(ProductName, "UnpackPA30")
#pragma compile(ProductVersion, 1.0.0)

; CmdLine
Dim $strInFile, $strOutFile, $strPatchFile
Dim $bNullDiff, $bDryRun, $bAllowLegacy

Dim $CmdLineParserNextIsParameter

Dim $i

Func ShowUsage()
  ; FIXME
EndFunc

; ----- Read the command line -----

If $CmdLine[0] < 4 Then
  ConsoleWrite("ERROR: Missing argument.")
  ShowUsage()
  Exit(1)
EndIf

$strInFile = ""
$strOutFile = ""
$strPatchFile = ""
$bNullDiff = False
$bDryRun = False
$bAllowLegacy = False

$CmdLineParserNextIsParameter = False

For $i = 1 To $CmdLine[0]
  If $CmdLineParserNextIsParameter = False Then
    Switch StringLower($CmdLine[$i])
      Case "-i", "/i", "-input", "/input", "--input", "//input"
        If $i < $CmdLine[0] Then
          $strInFile = $CmdLine[($i + 1)]
          $CmdLineParserNextIsParameter = True
        EndIf
      Case "-o", "/o", "-output", "/output", "--output", "//output"
        If $i < $CmdLine[0] Then
          $strOutFile = $CmdLine[($i + 1)]
          $CmdLineParserNextIsParameter = True
        EndIf
      Case "-p", "/p", "-patch", "/patch", "--patch", "//patch"
        If $i < $CmdLine[0] Then
          $strPatchFile = $CmdLine[($i + 1)]
          $CmdLineParserNextIsParameter = True
        EndIf
      Case "-n", "/n", "-null", "/null", "--null", "//null"
        $bNullDiff = True
      Case "-d", "/d", "-dry", "/dry", "--dry", "//dry"
        $bDryRun = True
      Case "-l", "/l", "-legacy", "/legacy", "--legacy", "//legacy"
        $bAllowLegacy = True
    EndSwitch
  Else
    $CmdLineParserNextIsParameter = False
  EndIf
Next

MsgBox(BitOr($MB_TASKMODAL, $MB_ICONINFORMATION, $MB_OK), "Information", "$strInFile = " & $strInFile & @LF & "$strOutFile = " & $strOutFile & @LF & "$strPatchFile = " & $strPatchFile & @LF & "$bNullDiff = " & $bNullDiff & @LF & "$bDryRun = " & $bDryRun & @LF & "$bAllowLegacy = " & $bAllowLegacy)

; ----- Validate the input / check possibility -----

; ----- Determine patch type (with or without CRC32) -----

; ----- Apply patch -----

Exit(0)