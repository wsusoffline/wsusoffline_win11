@echo off

verify other 2>nul
setlocal enableextensions enabledelayedexpansion
if errorlevel 1 goto Error

if "%DIRCMD%" NEQ "" set DIRCMD=

cd /D "%~dp0"

call .\CompileAutoItScripts.cmd

.\UnpackPA30.exe -n -p .\_work\historycix.cab.pa30 -o .\_work\historycix.cab


goto EoF
:Error
pause

:EoF
endlocal
