@echo off

verify other 2>nul
setlocal enableextensions enabledelayedexpansion
if errorlevel 1 goto Error

if "%DIRCMD%" NEQ "" set DIRCMD=

cd /D "%~dp0"

echo Determining script interpreter locations...
set CSCRIPT_PATH=%SystemRoot%\System32\cscript.exe
if not exist %CSCRIPT_PATH% goto Error
if /i "%PROCESSOR_ARCHITECTURE%"=="AMD64" (set WGET_PATH=.\bin\wget64.exe) else (
  if /i "%PROCESSOR_ARCHITEW6432%"=="AMD64" (set WGET_PATH=.\bin\wget64.exe) else (set WGET_PATH=.\bin\wget.exe)
)
if not exist %WGET_PATH% goto Error
echo.

echo Cleaning up...
if exist ".\_work" rd /s /q ".\_work"
echo.

echo Creating work-folder...
mkdir ".\_work"
echo.

echo Downloading files...
if exist ".\_raw\windows10.0-kb5007215-x64_0257394e095bfcaffd59f41dc30df4d94edbb7e2.cab" (
  copy ".\_raw\windows10.0-kb5007215-x64_0257394e095bfcaffd59f41dc30df4d94edbb7e2.cab" ".\_work" >nul
) else (
  %WGET_PATH% -nv -N -i ".\DownloadLinks-all.txt" -P ".\_work"
)
echo.

echo Determining inner archive search pattern...
rem The file name "windows10.0-kb5007215-x64_0257394e095bfcaffd59f41dc30df4d94edbb7e2.cab" will be read from UpdateTable-w110.csv
set UPDATETABLE_NAME=windows10.0-kb5007215-x64_0257394e095bfcaffd59f41dc30df4d94edbb7e2.cab
echo - UPDATETABLE_NAME=%UPDATETABLE_NAME%
set INNER_NAME_PATTERN=
for /f "tokens=1 delims=_" %%f in ('echo %UPDATETABLE_NAME%') do (
  if not "%%f"=="" (set INNER_NAME_PATTERN=%%f)
)
echo - INNER_NAME_PATTERN=%INNER_NAME_PATTERN%
echo.

echo Checking file names...
if "%UPDATETABLE_NAME%"=="%INNER_NAME_PATTERN%.cab" goto Error
echo.

echo Extracting inner archives...
%SystemRoot%\System32\expand.exe ".\_work\%UPDATETABLE_NAME%" -F:%INNER_NAME_PATTERN%.* ".\_work" >nul
del /Q ".\_work\%UPDATETABLE_NAME%"
echo.

echo Determining inner archive names...
set INNER_NAME_CAB=
set INNER_NAME_PSF=
dir /b ".\_work\%INNER_NAME_PATTERN%*.cab" >nul 2>&1
if errorlevel 1 goto Error
dir /b ".\_work\%INNER_NAME_PATTERN%*.psf" >nul 2>&1
if errorlevel 1 goto Error
for /f "delims=" %%f in ('dir /b ".\_work\%INNER_NAME_PATTERN%*.cab"') do (
  if not "%%f"=="" (set INNER_NAME_CAB=%%f)
)
if "%INNER_NAME_CAB%"=="" goto Error
for /f "delims=" %%f in ('dir /b ".\_work\%INNER_NAME_PATTERN%*.psf"') do (
  if not "%%f"=="" (set INNER_NAME_PSF=%%f)
)
if "%INNER_NAME_PSF%"=="" goto Error
if not exist ".\_work\%INNER_NAME_CAB%" goto Error
if not exist ".\_work\%INNER_NAME_PSF%" goto Error
echo - INNER_NAME_CAB=%INNER_NAME_CAB%
echo - INNER_NAME_PSF=%INNER_NAME_PSF%
echo.

echo Extracting express.psf.cix.xml...
%SystemRoot%\System32\expand.exe ".\_work\%INNER_NAME_CAB%" -F:express.psf.cix.xml ".\_work" >nul
if errorlevel 1 goto Error
echo.

echo Parsing express.psf.cix.xml...
if not exist ".\_work\express.psf.cix.xml" goto Error
%CSCRIPT_PATH% //Nologo //B //E:vbs .\cmd\XSLT.vbs ".\_work\express.psf.cix.xml" .\xslt\express_extract_offsets.xsl ".\_work\express_psf_offsets.txt"
if errorlevel 1 goto Error
if not exist ".\_work\express_psf_offsets.txt" goto Error
%CSCRIPT_PATH% //Nologo //B //E:vbs .\cmd\XSLT.vbs ".\_work\express.psf.cix.xml" .\xslt\express_extract_hashes_compressed.xsl ".\_work\express_hashes_compressed.txt"
if errorlevel 1 goto Error
if not exist ".\_work\express_hashes_compressed.txt" goto Error
%CSCRIPT_PATH% //Nologo //B //E:vbs .\cmd\XSLT.vbs ".\_work\express.psf.cix.xml" .\xslt\express_extract_hashes_uncompressed.xsl ".\_work\express_hashes_uncompressed.txt"
if errorlevel 1 goto Error
if not exist ".\_work\express_hashes_uncompressed.txt" goto Error
%CSCRIPT_PATH% //Nologo //B //E:vbs .\cmd\XSLT.vbs ".\_work\express.psf.cix.xml" .\xslt\express_extract_compression.xsl ".\_work\express_compressions.txt"
if errorlevel 1 goto Error
if not exist ".\_work\express_compressions.txt" goto Error
echo.

echo Extracting CAB archive...
mkdir ".\_work\expanded"
%SystemRoot%\System32\expand.exe ".\_work\%INNER_NAME_CAB%" -F:* ".\_work\expanded" >nul
if errorlevel 1 goto Error
echo.

rem rem Revision 1 (bulk parsing will follow)
rem echo Extracting files from PSF...
rem for /f "tokens=1,2,3 delims=," %%a in (.\_work\express_psf_offsets.txt) do (
rem   rem %%a = @name
rem   rem %%b = __:Delta/__:Source/@offset
rem   rem %%c = __:Delta/__:Source/@length
rem   
rem   echo - Extracting %%a...
rem   rem %CSCRIPT_PATH% //Nologo //E:vbs .\PartialFile.vbs ".\_work\%INNER_NAME_PSF%" ".\_work\expanded\%%a" %%b %%c
rem   %CSCRIPT_PATH% //Nologo //B //E:vbs .\PartialFile.vbs ".\_work\%INNER_NAME_PSF%" ".\_work\expanded\%%a" %%b %%c
rem   if errorlevel 1 goto Error
rem )

rem Revision 2
echo Extracting files from PSF...
rem %CSCRIPT_PATH% //Nologo //E:vbs .\PartialFileBulk.vbs ".\_work\%INNER_NAME_PSF%" ".\_work\expanded" ".\_work\express_psf_offsets.txt"
%CSCRIPT_PATH% //Nologo //B //E:vbs .\PartialFileBulk.vbs ".\_work\%INNER_NAME_PSF%" ".\_work\expanded" ".\_work\express_psf_offsets.txt"
if errorlevel 1 goto Error



rem FIXME: run hashdeep on PSF
rem FIXME: unpack PA30
rem FIXME: run hashdeep on unpacked files
rem FIXME: install resulting directory










goto EoF
:Error
pause

:EoF
endlocal
