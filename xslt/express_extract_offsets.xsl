<?xml version="1.0"?>
<!--
     Author: H. Buhrmester, 2021
             aker, 2021-2022
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:__="urn:ContainerIndex" version="1.0">
  <xsl:output omit-xml-declaration="yes" indent="no" method="text" />
  <xsl:template match="/">
    <xsl:for-each select="__:Container/__:Files/__:File">
      <!-- file name and relative path -->
      <xsl:value-of select="@name" />
      <xsl:text>,</xsl:text>
	  <!-- offset inside the PSF file -->
      <xsl:value-of select="__:Delta/__:Source/@offset" />
      <xsl:text>,</xsl:text>
	  <!-- file size (compressed) -->
      <xsl:value-of select="__:Delta/__:Source/@length" />
      <xsl:text>&#10;</xsl:text>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
